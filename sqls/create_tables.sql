CREATE TABLE [dbo].[Steam_app_details](
[appid] [int] NOT NULL,
[name] [varchar](max) NOT NULL,
[score_rank] [varchar](50) NOT NULL,
[positive] [int] NOT NULL,
[negative] [int] NOT NULL,
[userscore] [int] NOT NULL,
[owners] [varchar](50) NOT NULL,
[average_forever] [int] NOT NULL,
[average_2weeks] [int] NOT NULL,
[median_forever] [int] NOT NULL,
[median_2weeks] [int] NOT NULL,
[price] [int] NOT NULL,
[initialprice] [int] NOT NULL,
[discount] [int] NOT NULL,
[ccu] [int] NOT NULL,
PRIMARY KEY CLUSTERED
(
[appid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [dbo].[Steam_app_player_count](
[appid] [int] NULL,
[player_count] [bigint] NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[Steam_apps](
[appid] [int] NOT NULL,
[name] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

