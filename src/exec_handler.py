
import steam_api, data_layer, proxies_handler
import time, math, threading


class ExecHandler():
    exec_obj = None
    api_obj = None
    dl_obj = None
    ph_obj = None
    objects_methods = None
    data_block_size = None


    def __init__(self, exec_obj, db_data, data_block_size):
        self.exec_obj = exec_obj
        self.dl_obj = data_layer.DataLayer(db_data)
        self.ph_obj = proxies_handler.ProxiesHandler()
        self.data_block_size = data_block_size
        # APP_LIST; APP_DETAILS; CURR_PLAYERS_NUM
        self.objects_methods = {
            "app_list": {
                "get": self.get_app_list,
                "insert": self.dl_obj.insert_steam_apps,
                "update": self.dl_obj.update_steam_apps,
                "check": self.dl_obj.check_steam_apps_appid
            },
            "app_details": {
                "get": self.get_app_details,
                "insert": self.dl_obj.insert_steam_app_details,
                "update": self.dl_obj.update_steam_app_details,
                "check": self.dl_obj.check_steam_app_details_appid
            },
            "curr_players_num": {
                "get": self.get_curr_players_num,
                "insert": self.dl_obj.insert_steam_app_player_count,
                "update": self.wrp_update_steam_app_player_count,
                "check": self.dl_obj.check_steam_app_player_count_appid
            }
        }


    def wrp_update_steam_app_player_count(self, data):
        resp = dl_obj.selet_player_count(data["appid"])
        data["player_count"] = str( int(data["player_count"]) + int(resp["player_count"]) )
        self.dl_obj.update_steam_app_player_count(data)


    def get_sublists(self, a_list, divisions):
        total = len(a_list)
        subq = int(total / divisions)
        new_list = list()
        ini = 0
        for div in range(0, divisions):
            new_list.append( list() )
            for i in range(ini, ini + subq):
                new_list[div].append(a_list[i])
            ini += subq
        for i in range(ini, total):
            new_list[len(new_list)-1].append(a_list[i])
        return new_list


    def insert_update_handler(self, methods, data_block):
        count = 0
        # print("data_block:" + str(data_block))
        for data in data_block:
            if(methods["check"](data["appid"])):
                methods["update"](data)
                count += 1
            else:
                methods["insert"](data)
                count += 1
            self.dl_obj.insert_processed_apps(self.exec_obj, data["appid"])
        return count


    def get_app_list(self, proxies_list):
        print("get_app_list")
        api_obj = steam_api.SteamApi()
        data = api_obj.get_app_list()
        self.loop_over_data(self.objects_methods[self.exec_obj], data)


    # method: app_details;curr_players_num
    def loop_over_app_list(self, proxies_list, method):
        print("loop_over_app_list...")
        proxy_in_use = 0
        if(len(proxies_list) > 0):
            api_obj = steam_api.SteamApi(proxy = proxies_list[proxy_in_use]["proxy"])
        else:
            api_obj = steam_api.SteamApi()
        get_methods = {"app_details": api_obj.get_app_details, "curr_players_num": api_obj.get_current_number_of_players}
        # data = self.dl_obj.select_steam_apps()
        data = self.dl_obj.select_steam_apps_not_processed(self.exec_obj, self.data_block_size)
        if(len(data) == 0):
            self.dl_obj.clear_processed_apps(self.exec_obj)
            data = self.dl_obj.select_steam_apps_not_processed(self.exec_obj, self.data_block_size)
        n_sub_lists = math.ceil(len(data) / 100)
        print("data:" + str(len(data)) + ";n_sub_lists:" + str(n_sub_lists))
        data_sublisted = self.get_sublists(data, n_sub_lists)
        resp = list()
        for sublist in data_sublisted:
            print("init sublist")
            for d in sublist:
                try:
                    print("app->" + str(d))
                    new_reg = get_methods[method](d["appid"])
                    resp.append(new_reg)
                    time.sleep(5)
                except Exception as ex:
                    print("\t" + str(ex))
                    proxy_in_use += 1
                    if(proxy_in_use < len(proxies_list)):
                        del api_obj
                        api_obj = steam_api.SteamApi(proxy = proxies_list[proxy_in_use]["proxy"])
                    else:
                        print("all proxies blocked!!")
                        self.dl_obj.close()
                        break;
            print("end of sublist")
            self.loop_over_data(self.objects_methods[self.exec_obj], resp)


    def get_app_details(self, proxies_list):
        print("get_app_details")
        self.loop_over_app_list(proxies_list, "app_details")


    def get_curr_players_num(self, proxies_list):
        print("get_curr_players_num")
        self.loop_over_app_list(proxies_list, "curr_players_num")


    def loop_over_data(self, methods, data, limit = 100):
        print("loop_over_data...")
        data_block = list()
        count = 0
        for d in data:
            data_block.append(d)
            count += 1
            if(count >= limit):
                self.insert_update_handler(methods, data_block.copy())
                data_block.clear()
                count = 0
        return count

    # methods = {"insert": self.dl_obj.insert_steam_apps, "update": self.dl_obj.update_steam_apps, "check": self.dl_obj.check_steam_apps_appid}
    def exec_prog(self, proxies_to_use):
        print("exec_prog")
        proxies_list = list()
        # if(proxies_to_use > 0):
        proxies_list = self.ph_obj.get_random_n_proxies( proxies_to_use )
        self.objects_methods[self.exec_obj]["get"](proxies_list)
