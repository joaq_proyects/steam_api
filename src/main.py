
import schedule
import exec_handler, settings

def exec_steam_apps():
    eh_obj = exec_handler.ExecHandler("app_list", settings.db_data, settings.data_block_size)
    eh_obj.exec_prog(settings.proxies_to_use)
    eh_obj = exec_handler.ExecHandler("app_details", settings.db_data, settings.data_block_size)
    eh_obj.exec_prog(settings.proxies_to_use)

def exec_steam_app_player_count():
    eh_obj = exec_handler.ExecHandler("curr_players_num", settings.db_data, settings.data_block_size)
    eh_obj.exec_prog(settings.proxies_to_use)

objects = settings.objects
objects["steam_apps"]["exec_func"]: exec_steam_apps
objects["steam_app_player_count"]["exec_func"]: exec_steam_app_player_count

#Execute first, then scheduled
# exec_steam_apps()
exec_steam_app_player_count()

for obj in objects:
    print("obj:" + str(type(obj)) + ":" + str(obj))
    if(objects[obj]["exec_type"] == "daily"):
        schedule.every().day.at(objects[obj]["exec_hour"]).do(objects[obj]["exec_func"])
    elif(objects[obj]["exec_type"] == "hourly"):
        schedule.every(1).hour.do(objects[obj]["exec_func"])

while(True):
    schedule.run_pending()
    time.sleep(30)
