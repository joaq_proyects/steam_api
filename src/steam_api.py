
import json
import requests

class SteamApi():

    proxy = None

    def __init__(self, proxy = {}):
        self.proxy = proxy


    def make_request(self, url):
        res = requests.get( url, proxies=self.proxy, timeout=120 )
        result = dict()
        if(res.status_code == 200):
            result = json.loads(res.text)
        else:
            raise Exception("Error when requesting url: " + str(res.status_code) + ":" + str(res.text))
        return result


    def get_app_list(self):
        url = "http://api.steampowered.com/ISteamApps/GetAppList/v2"
        res = self.make_request(url)
        return res["applist"]["apps"]


    def get_app_details(self, appid):
        url = "https://steamspy.com/api.php?request=appdetails&appid=" + str(appid)
        res = self.make_request(url)
        return res


    def get_current_number_of_players(self, appid):
        url = "https://api.steampowered.com/ISteamUserStats/GetNumberOfCurrentPlayers/v1?appid=" + str(appid)
        res = self.make_request(url)
        return res["response"]["result"]
