
import data_access

class DataLayer():

    da_obj = None


    def __init__(self, db_data):
        self.da_obj = data_access.DataAccess( db_data["host"], db_data["user"], db_data["pssw"], db_data["db"] )
        print("New Connection:" + str(self.da_obj))


    def __del__(self):
        if(self.da_obj is not None):
            self.da_obj.close()


    def close(self):
        del self
        print("Connection Closed")


    def replace_tags(self, text, tag, data):
        data_keys = data.keys()
        for dk in data_keys:
            replace_with = str(data[dk]).replace("'", "''").replace("None", "0")
            if(len(replace_with) == 0):
                replace_with = "null"
            text = text.replace(tag + dk + tag, replace_with)
        return text


    def exec_insert(self, query):
        try:
            self.da_obj.exec_non_query(query)
            self.da_obj.commit()
        except Exception as ex:
            print("Error en:\r\n" + str(query) + "\r\n" + str(ex))


    def exec_update(self, query):
        self.da_obj.exec_non_query(query)
        self.da_obj.commit()


    # -------steam_apps queries-------

    def insert_steam_apps(self, data):
        query = """
        insert into steam_apps(
        	appid,
        	name
        )
        values(
        	##appid##,
        	'##name##'
        )
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_insert(query)

    def update_steam_apps(self, data):
        query = """
        update steam_apps
        set name = '##name##'
        where appid = ##appid##
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_update(query)

    def select_steam_apps(self):
        query = "select appid as \"appid\", name as \"name\" from steam_apps"
        table = self.da_obj.exec_query(query)
        resp = list()
        for reg in table:
            resp.append({"appid":reg[0],"name":reg[1]})
        return resp

    def check_steam_apps_appid(self, appid):
        query = "select 1 from steam_apps where appid = " + str(appid)
        table = self.da_obj.exec_query(query)
        return (len(table) > 0)


    # -------steam_app_details queries-------

    def insert_steam_app_details(self, data):
        query = """
        insert into steam_app_details(
        	appid,
        	name,
        	score_rank,
        	positive,
        	negative,
        	userscore,
        	owners,
        	average_forever,
        	average_2weeks,
        	median_forever,
        	median_2weeks,
        	price,
        	initialprice,
        	discount,
        	ccu
        )
        values(
        	##appid##,
        	'##name##',
        	'##score_rank##',
        	##positive##,
        	##negative##,
        	##userscore##,
        	'##owners##',
        	##average_forever##,
        	##average_2weeks##,
        	##median_forever##,
        	##median_2weeks##,
        	##price##,
        	##initialprice##,
        	##discount##,
        	##ccu##
        )
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_insert(query)


    def update_steam_app_details(self, data):
        query = """
        update steam_app_details
        set name = '##name##',
        	score_rank = '##score_rank##',
        	positive = ##positive##,
        	negative = ##negative##,
        	userscore = ##userscore##,
        	owners = '##owners##',
        	average_forever = ##average_forever##,
        	average_2weeks = ##average_2weeks##,
        	median_forever = ##median_forever##,
        	median_2weeks = ##median_2weeks##,
        	price = ##price##,
        	initialprice = ##initialprice##,
        	discount = ##discount##,
        	ccu = ##ccu##
        where appid = ##appid##
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_update(query)


    def check_steam_app_details_appid(self, appid):
        query = "select 1 from steam_app_details where appid = " + str(appid)
        table = self.da_obj.exec_query(query)
        return (len(table) > 0)


    def select_steam_apps_not_processed(self, exec_obj, top = 100):
        table = ""
        if(exec_obj == "app_details"):
            table = "processed_apps_details"
        elif(exec_obj == "curr_players_num"):
            table = "processed_apps_counts"
        query = "select top " + str(top) + " appid as \"appid\", name as \"name\" from steam_apps where appid not in (select appid from " + table + ")"
        table = self.da_obj.exec_query(query)
        resp = list()
        for reg in table:
            resp.append({"appid":reg[0],"name":reg[1]})
        return resp


    def insert_processed_apps(self, exec_obj, appids):
        table = ""
        if(exec_obj == "app_details"):
            table = "processed_apps_details"
        elif(exec_obj == "curr_players_num"):
            table = "processed_apps_counts"
        query = "insert into " + table + "(appid) values"
        values = ""
        for appid in appids:
            values += ",( " + appid + ")"
        query += values[1:]
        self.da_obj.exec_non_query(query)
        self.da_obj.commit()


    def clear_processed_apps(self, exec_obj):
        table = ""
        if(exec_obj == "app_details"):
            table = "processed_apps_details"
        elif(exec_obj == "curr_players_num"):
            table = "processed_apps_counts"
        query = "delete from " + table
        self.da_obj.exec_non_query(query)
        self.da_obj.commit()


    # -------steam_app_player_count queries-------

    def insert_steam_app_player_count(self, data):
        query = """
        insert into steam_app_player_count(
        	appid,
        	player_count
        )
        values(
        	##appid##,
        	##player_count##
        )
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_insert(query)


    def update_steam_app_player_count(self, data):
        query = """
        update steam_app_player_count
        set player_count = ##player_count##
        where appid = ##appid##
        """
        query = self.replace_tags(query, "##", data)
        return self.exec_update(query)

    def selet_player_count(self, appid):
        query = "select player_count from steam_app_player_count where appid = " + str(appid)
        table = self.da_obj.exec_query(query)
        resp = list()
        for reg in table:
            resp.append({"player_count":reg[0]})
        return resp


    def check_steam_app_player_count_appid(self, appid):
        query = "select 1 from steam_app_player_count where appid = " + str(appid)
        table = self.da_obj.exec_query(query)
        return (len(table) > 0)
