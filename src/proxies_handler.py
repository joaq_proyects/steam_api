
import json, random, time
import requests

class ProxiesHandler:

    proxies_list = None
    type_of_proxy = None

    def __init__(self):
        self.type_of_proxy = None
        self.proxies_list = list()
        self.proxy_list_get_method = self.proxy_list_by_settings
        self.load_proxies_list()
        self.check_proxies_state()


    def proxy_list_by_settings(self):
        import settings
        self.type_of_proxy = "https"
        return settings.proxies_list


    def check_proxies_state(self):
        if(self.proxies_list is not None):
            for p in self.proxies_list:
                p["in_use"] = False
                # IMPLEMENT SOMETHING TO CHECK HEALTH


    def load_proxies_list(self):
        aux_list = self.proxy_list_get_method()
        count = 0
        for prx in aux_list:
            count += 1
            proxy = {self.type_of_proxy: prx.replace('\r','')}
            self.proxies_list.append( {"id":count,"proxy":proxy,"active":True,"in_use":False} )


    def get_random_n_proxies( self, p_cant ):
        max_pos = len(self.proxies_list) - 1
        proxies_groups = list()
        aux_count = 0
        if(max_pos >= p_cant):
            while(len(proxies_groups) < p_cant and aux_count <= max_pos):
                aux_count += 1
                pos = random.randint(0, max_pos)
                if(self.proxies_list[pos]["active"] and not self.proxies_list[pos]["in_use"]):
                    proxies_groups.append(self.proxies_list[pos])
                    self.proxies_list[pos]["in_use"] = True
        else:
            print("arg 1 length must be lower than arg 2 (%s)" % (str(max_pos) + "<=" + str(p_cant),))
        # print("proxies_groups:" + str(proxies_groups))
        return proxies_groups
